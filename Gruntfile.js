module.exports = function(grunt) {

    require('time-grunt')(grunt);

    //-------------------------------------------------------------------
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        cssFolder:'css',
        runTarget: 'landing_page.html',
        cssmin: {
            options: {
            },

            combine: {
                files: [{
                    expand: true,
                    cwd: '<%= cssFolder %>',
                    src: ['*.css'],
                    dest: 'dest/css',
                    ext: '.min.css'
                }]
            }
        },
        watch: {
            scripts: {
                files: ['css/*.less'],
                tasks: ['less','cssmin'],
                options: {
                    spawn: false,
                    livereload: true
                }
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        './**/*.css',
                        './**/*.html',
                        './**/*.js',
                        '!node_modules/**/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    open: false,
                    port:3061,
                    server: {
                        baseDir: "./",
                        index: "<%= runTarget %>"
                    }
                }
            }
        },
        clean: {
            css: ["css/*.css"]
        },
        less: {
            development: {
                options: {
                    paths: ["css"],
                    cleancss: true
                },
                files: {
                    "<%= cssFolder %>/landing_page.css":
                      [ "<%= cssFolder %>/config.less",
                          "<%= cssFolder %>/landing_page.less",
                      ],
                    "<%= cssFolder %>/step1.css":
                      [ "<%= cssFolder %>/config.less",
                          "<%= cssFolder %>/progressbar.less",
                          "<%= cssFolder %>/awesome-bootstrap-checkbox.less",
                          "<%= cssFolder %>/step1.less",
                      ],
                }
            }
        }
    });
    //-------------------------------------------------------------------
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-less');
    //-------------------------------------------------------------------

    grunt.registerTask('dev', ['browserSync', 'watch']);
    grunt.registerTask('build', ['less', 'cssmin','clean']);

    grunt.registerTask('run', 'Start a custom web server', function(mode) {
        var target;
        switch (mode){
            case 'step1':
                target='step1.html';
                break;
            case 'step2':
                target='step2.html';
                break;
            case 'step3':
                target='step3.html';
                break;
            default:
                target='landing_page.html';
        }
        // target='landing_page.html';
        grunt.config.set('runTarget', target);
        grunt.task.run('dev');
    });

};
