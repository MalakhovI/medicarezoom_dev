# medicarezoom

## Run
 - Grunt should be install globally `npm install -g grunt-cli`.
 - run `npm i` in project folder.
 - run it:
    - `grunt run` - 'landing page' page
    - `grunt run:step1` - 'step1 page' page
    - `grunt run:step2` - 'step2 page' page
    - `grunt run:step3` - 'step3 page' page

 grunt run simple server for page.
 You can see something like this in console:

```
[BS] Access URLs:
 --------------------------------------
       Local: http://localhost:3061
    External: http://192.168.1.106:3061

```


 You can use IP from console (example: 192.168.1.106:3061) to connect your mobile device.
 
 Your mobile device should be in the same network which server.
